// console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let firstName = "Jan Jonell";
	console.log("First Name:" + firstName);

	let lastName = "mendoza";
	console.log("Last Name:" + lastName);

	let age1 = 32;
	console.log("My current age is: " + age1);

	let hobbies = ["basketball","valorant","dota2","NBA2k2022","Tekken","Watching movies" ,"chess"];
	console.log("My hobbies are: ")
	console.log(hobbies);

	let workAddress = {

		houseNumber: 1012,
		street: "brgy del remedio",
		city:"san pablo city",
		state: "laguna"

	}

	console.log("Work Address:")
	console.log(workAddress)



	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);
